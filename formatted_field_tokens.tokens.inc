<?php

/**
 * @file
 * Token functions for Formatted Field token module.
 */

/**
 * Implements hook_token_info().
 */
function formatted_field_tokens_token_info() {
  $type = array(
    'name'        => t('Formatted Field'),
    'description' => t('Formatted Field tokens.'),
    'needs-data'  => 'entity',
  );

  $tokens['?'] = array(
    'name'        => t('Formatted Field token'),
    'description' => t('The Formatted Field token provides one or many field values rendered via the default or specified field formatter.<br />The format is [PREFIX:DELTA(S):FORMATTER:FORMATTER_SETTING_KEY-FORMATTER_SETTING_VALUE:...] (e.g. [node:field_image:0,1:image:image_style-thumbnail]).'),
  );

  return array(
    'types'  => array(
      'formatted-field' => $type
    ),
    'tokens' => array(
      'formatted-field' => $tokens
    ),
  );
}

/**
 * Implements hook_token_info_alter().
 */
function formatted_field_tokens_token_info_alter(&$data) {
  $entity_info = entity_get_info();
  $fields      = field_info_fields();
  foreach ($entity_info as $entity_type => $info) {
    if (isset($info['token type']) && isset($data['tokens'][$info['token type']])) {
      foreach ($fields as $field) {
        if (in_array($entity_type, $field['entity_types']) && isset($data['tokens'][$info['token type']][$field['field_name']])) {
          $data['tokens'][$info['token type']]["{$field['field_name']}-formatted"] = $data['tokens'][$info['token type']][$field['field_name']];
          $data['tokens'][$info['token type']]["{$field['field_name']}-formatted"]['type'] = 'formatted-field';
        }
      }
    }
  }
}

/**
 * Implements hook_tokens().
 */
function formatted_field_tokens_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options   = array('absolute' => TRUE);
  $language_code = NULL;
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code           = $options['language']->language;
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'entity' && !empty($data['entity'])) {
    $entity_type = $data['entity_type'];
    list($id, $vid, $bundle_name) = entity_extract_ids($entity_type, $data['entity']);
    $instances  = field_info_instances($entity_type, $bundle_name);
    $formatters = field_info_formatter_types();

    foreach ($instances as $field_name => $instance) {
      $field_tokens = token_find_with_prefix($tokens, "{$field_name}-formatted");
      if ($field_tokens) {
        foreach ($field_tokens as $name => $original) {
          $entity = clone $data['entity'];

          // Token info.
          $parts     = explode(':', $name);
          $deltas    = explode(',', array_shift($parts));
          $formatter = array_shift($parts);

          $formatter_settings = array();
          foreach ($parts as $formatter_setting) {
            list($name, $value) = explode('-', $formatter_setting);
            $formatter_settings[$name] = $value;
          }
          if (!empty($formatters[$formatter]['settings'])) {
            $formatter_settings += $formatters[$formatter]['settings'];
          }

          // Field info.
          $field    = field_info_field($field_name);
          $langcode = field_language($entity_type, $entity, $field_name);
          $display  = $instance['display']['default'];
          $items    = field_get_items($entity_type, $entity, $field_name);

          if (!empty($items)) {
            // Ensure that all referenced deltas exist.
            $diff = array_diff(array_values($deltas), array_keys($items));
            if (empty($diff)) {
              // Adjust Formatter.
              if (!is_null($formatter) && isset($formatters[$formatter]) && in_array($field['type'], $formatters[$formatter]['field types'])) {
                $display['type']     = $formatter;
                $display['settings'] = isset($formatter_settings) ? $formatter_settings : $formatters[$formatter]['settings'];
              }

              $token_items = array();
              foreach ($deltas as $delta) {
                $token_items[] = $items[$delta];
              }

              // Invoke MODULE_field_load().
              $function = "{$field['module']}_field_load";
              if (function_exists($function)) {
                $items = array($token_items);
                $function($entity_type, array($entity), $field, array($instance), $langcode, $items, FIELD_LOAD_CURRENT);
                $token_items = $items[0];
              }

              // Invoke MODULE_field_is_empty().
              foreach ($token_items as $item) {
                $function = "{$field['module']}_field_is_empty";
                if (function_exists($function) && $function($item, $field)) {
                  continue(2);
                }
              }

              // Prepare entity with amended field items.
              $entity->{$field_name}[$langcode] = $token_items;

              // Render formatted field.
              $element = field_view_field($entity_type, $entity, $field_name, $display, $langcode);
              if ($element) {
                $replacements[$original] = '';
                foreach (element_children($element) as $delta) {
                  $replacements[$original] .= render($element[$delta]);
                }
              }
            }
          }
        }
      }
    }
  }

  return $replacements;
}
