# Formatted Field tokens
------------------------

The Formatted Field tokens module adds chained tokens allowing one or many
field values to be rendered via the default or specified field formatter.

The format is:
```
[PREFIX:DELTA(S):FORMATTER:FORMATTER_SETTING_KEY-FORMATTER_SETTING_VALUE:...]
```

(e.g. **[node:field_image-formatted:0,1:image:image_style-thumbnail]**).



## Recommended modules
----------------------

- [Token filter](https://www.drupal.org/project/token_filter)


## TODO
-------

- Add tests.
- Add help module integration.
